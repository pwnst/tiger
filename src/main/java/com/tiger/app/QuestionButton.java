package com.tiger.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class QuestionButton extends JButton {

    private Question question;

    private JFrame frame;

    private ActionListener actionListener;

    private JTextArea questionArea;

    private List<AnswerButton> answerButtons = new ArrayList<>();

    private List<QuestionButton> questionButtons = new ArrayList<>();

    private QuestionButton(
            JFrame frame,
            Question question,
            List<AnswerButton> answerButtons,
            List<QuestionButton> questionButtons,
            JTextArea questionArea
    ) {
        super(String.valueOf(question.getValue()));
        this.frame = frame;
        this.question = question;
        this.answerButtons = answerButtons;
        this.questionButtons = questionButtons;
        this.questionArea = questionArea;
    }

    public static QuestionButton of(
            Rectangle rectangle,
            Question question,
            JFrame frame,
            List<AnswerButton> answerButtons,
            List<QuestionButton> questionButtons,
            JTextArea questionArea
    ) {
        QuestionButton questionButton = new QuestionButton(frame, question, answerButtons, questionButtons, questionArea);
        questionButton.setBounds(rectangle);
        questionButton.setBackground(Main.SAND);
        questionButton.setForeground(Color.WHITE);
        questionButton.setFont(new Font(Main.FONT, 0, 24));
        questionButton.setActionListener();
        questionButton.enableButton();
        return questionButton;
    }

    public void setActionListener() {
        this.actionListener = e -> answerButtons();
    }

    public void disableButton() {
        setFocusPainted(false);
        setBorderPainted(false);
        removeActionListener(actionListener);
    }

    public void enableButton() {
        Boolean correctAnswer = question.isCorrectAnswer();
        if (isNull(correctAnswer)) {
            setFocusPainted(true);
            setBorderPainted(true);
            addActionListener(actionListener);
        } else {
            setFocusPainted(false);
            setBorderPainted(false);
            if (correctAnswer) {
                setBackground(Main.GREEN);
            } else {
                setBackground(Main.RED);
            }
        }
    }

    private void answerButtons() {
        answerButtons.forEach(frame::remove);
        questionArea.setText(question.getQuestion());
        List<String> answers = question.getAnswers();
        int idx = 0;

        for (int y = 0; y < 2; y++) {
            for (int x = 0; x < 2; x++) {
                String answer = answers.get(idx++);

                int initX = 100;
                int initY = 550;
                int width = 500;
                int height = 40;

                Rectangle rectangle = new Rectangle(initX + width * x, initY + height * y, width, height);
                AnswerButton answerButton = AnswerButton.of(answer, rectangle, question, frame, answerButtons, questionButtons);
                answerButtons.add(answerButton);
                frame.add(answerButton);
            }
        }
        questionButtons.forEach(QuestionButton::disableButton);
        SwingUtilities.updateComponentTreeUI(frame);
    }
}
