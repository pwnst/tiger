package com.tiger.app;

import java.util.List;

import static java.util.Objects.nonNull;

public class Question {

    private Integer value;
    private String question;
    private List<String> answers;
    private Integer correctAnswerIndex;
    private Boolean correctAnswer;

    public Question(Integer value, String question, List<String> answers, Integer correctAnswerIndex) {
        this.value = value;
        this.question = question;
        this.answers = answers;
        this.correctAnswerIndex = correctAnswerIndex;
    }

    public Integer getValue() {
        return value;
    }

    public String getQuestion() {
        return question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public Integer getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }

    public Boolean isCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(Boolean correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Integer getScore() {
        if (nonNull(correctAnswer)) {
            return isCorrectAnswer() ? value : -value;
        } else {
            return 0;
        }
    }
}
