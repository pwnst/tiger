package com.tiger.app;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowAdapterImpl extends WindowAdapter {

    private JFrame frame;

    public WindowAdapterImpl(JFrame frame) {
        this.frame = frame;
    }

    @Override
    public void windowClosing(WindowEvent e) {
        frame.dispose();
    }
}
