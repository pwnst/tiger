package com.tiger.app;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class AnswerButton extends JButton {

    private Question question;

    private JFrame frame;

    private ActionListener actionListener;

    private boolean isCorrect;

    private boolean isAnswered;

    private List<AnswerButton> answerButtons = new ArrayList<>();

    private List<QuestionButton> questionButtons = new ArrayList<>();

    private AnswerButton(
            String answer,
            JFrame frame,
            Question question,
            List<AnswerButton> answerButtons,
            List<QuestionButton> questionButtons
    ) {
        super(answer);
        this.isCorrect = question.getAnswers().indexOf(answer) == question.getCorrectAnswerIndex();
        this.isAnswered = false;
        this.frame = frame;
        this.question = question;
        this.answerButtons = answerButtons;
        this.questionButtons = questionButtons;
    }

    public static AnswerButton of(
            String answer,
            Rectangle rectangle,
            Question question,
            JFrame frame,
            List<AnswerButton> answerButtons,
            List<QuestionButton> questionButtons
    ) {
        AnswerButton answerButton = new AnswerButton(answer, frame, question, answerButtons, questionButtons);
        answerButton.setBounds(rectangle);
        answerButton.setActionListener();
        answerButton.enableButton();
        answerButton.setBackground(Main.SAND);
        return answerButton;
    }

    public void setActionListener() {
        this.actionListener = e -> handleAnswer();
    }

    public void disableButton() {
        setFocusPainted(false);
        setBorderPainted(false);
        removeActionListener(actionListener);
        if (isAnswered) {
            if (isCorrect) {
                setBackground(Main.GREEN);
            } else {
                setBackground(Main.RED);
            }
        }
    }

    public void enableButton() {
        addActionListener(actionListener);
    }

    private void handleAnswer() {
        this.isAnswered = true;
        answerButtons.forEach(AnswerButton::disableButton);
        question.setCorrectAnswer(isCorrect);
        questionButtons.forEach(QuestionButton::enableButton);
        Main.updateScore();
        SwingUtilities.updateComponentTreeUI(frame);
    }
}
