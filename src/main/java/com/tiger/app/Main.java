package com.tiger.app;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class Main {

    public static List<AnswerButton> answerButtons = new ArrayList<>();
    public static List<QuestionButton> questionButtons = new ArrayList<>();
    public static JTextArea questionArea;
    public static JButton scoreArea;
    public static List<Category> categories = new ArrayList<>();
    public static JFrame frame;
    public static File selectedFile;

    public static final Color GREEN = new Color(0, 204, 102);
    public static final Color RED = new Color(255, 153, 153);
    public static final Color SAND = new Color(255, 230, 204);

    public static final String FONT = Font.SANS_SERIF;

    private static final String SCORE = "SCORE: %s";

    private static final int INIT_QUESTION_X = 250;
    private static final int INIT_QUESTION_Y = 50;
    private static final int QUESTION_WIDTH = 120;
    private static final int QUESTION_HEIGHT = 70;

    private static final int INIT_CATEGORY_X = 50;
    private static final int INIT_CATEGORY_Y = 50;
    private static final int CATEGORY_WIDTH = 150;
    private static final int CATEGORY_HEIGHT = 70;

    private static final int MARGIN = 5;

    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    private void init() {
        frame = new JFrame();
        frame.setSize(1200, 700);
        frame.setResizable(false);

        setBackground();
        playMusic();

        MenuBar mb = new MenuBar();
        Menu menu = new Menu("Menu");
        MenuItem openNew = new MenuItem("new");
        MenuItem restart = new MenuItem("restart");
        restart.addActionListener(e -> restart());
        openNew.addActionListener(e -> open());
        menu.add(openNew);
        menu.add(restart);
        mb.add(menu);
        frame.setMenuBar(mb);

        frame.addWindowListener(new WindowAdapterImpl(frame));

        frame.setLayout(null);
        frame.setVisible(true);
    }

    private void restart() {
        List<Category> categories = FileHandler.get(selectedFile);
        restart(categories);
    }

    private void restart(List<Category> categories) {
        frame.removeAll();
        frame.dispose();
        answerButtons.clear();
        questionButtons.clear();
        init();
        start(categories);
    }

    private void open() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setSelectedFile(new File(""));
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        if (chooser.showOpenDialog(frame) == JFileChooser.OPEN_DIALOG) {
            selectedFile = chooser.getSelectedFile();
            List<Category> categories = FileHandler.get(selectedFile);
            restart(categories);
        }
    }

    private void start() {
        start(FileHandler.get());
    }

    // validate, size, required fields, same name!
    private void start(List<Category> categories) {
        Main.categories = categories;

        setQuestionArea();
        setScoreArea();
        setCategories();
        setQuestionButtons();

        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void updateScore() {
        int score = categories.stream()
                .flatMap(Category::questionStream)
                .map(Question::getScore)
                .mapToInt(Integer::intValue).sum();
        scoreArea.setText(format(SCORE, score));
    }

    private void setCategories() {
        for (int y = 0; y < categories.size(); y++) {
            JButton button = new JButton(categories.get(y).getName());

            int yy = INIT_CATEGORY_Y + CATEGORY_HEIGHT * y + MARGIN * y;
            button.setBounds(INIT_CATEGORY_X, yy, CATEGORY_WIDTH, CATEGORY_HEIGHT);
            button.setFont(new Font(FONT, 0, 16));
            button.setBackground(SAND);
            button.setBorderPainted(false);
            button.setFocusPainted(false);

            frame.add(button);
        }
    }

    private void setQuestionButtons() {
        int ySize = categories.size();
        for (int y = 0; y < ySize; y++) {
            int xSize = categories.get(y).getQuestions().length;
            for (int x = 0; x < xSize; x++) {
                int xx = INIT_QUESTION_X + QUESTION_WIDTH * x + MARGIN * x;
                int yy = INIT_QUESTION_Y + QUESTION_HEIGHT * y + MARGIN * y;

                Rectangle rectangle = new Rectangle(xx, yy, QUESTION_WIDTH, QUESTION_HEIGHT);
                Question question = categories.get(y).getQuestions()[x];
                QuestionButton questionButton = QuestionButton.of(
                        rectangle,
                        question,
                        frame,
                        answerButtons,
                        questionButtons,
                        questionArea
                );
                questionButtons.add(questionButton);
                frame.add(questionButton);
            }
        }
    }

    private void setScoreArea() {
        scoreArea = new JButton();
        scoreArea.setBounds(500, 450, 200, 30);
        scoreArea.setFont(new Font(FONT, 1, 24));
        scoreArea.setBackground(SAND);
        scoreArea.setHorizontalAlignment(SwingConstants.HORIZONTAL);
        scoreArea.setBorderPainted(false);
        scoreArea.setFocusPainted(false);
        frame.add(scoreArea);
        updateScore();
    }

    private void setQuestionArea() {
        questionArea = new JTextArea();
        questionArea.setLineWrap(true);
        questionArea.setEditable(false);
        questionArea.setBounds(150, 500, 900, 30);
        questionArea.setBackground(SAND);
        questionArea.setFont(new Font(FONT, 0, 12));
        frame.add(questionArea);
    }

    private void setBackground() {
        URL resource = getClass().getResource("/background.jpg");
        try {
            frame.setContentPane(new JLabel(new ImageIcon(ImageIO.read(resource))));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void playMusic() {
        URL resource = getClass().getResource("/1.wav");
        AudioInputStream audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(resource);
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

}
