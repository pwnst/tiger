package com.tiger.app;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FileHandler {
    public static void main(String[] args) {
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<ArrayList<Category>>(){}.getType();
            URL resource = FileHandler.class.getResource("test.json");
            JsonReader jsonReader = new JsonReader(new FileReader(resource.getFile()));
            List<Category> o = gson.fromJson(jsonReader, listType);
            System.out.println("s");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static List<Category> get(){
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<ArrayList<Category>>(){}.getType();
            URL resource = FileHandler.class.getResource("test.json");
            JsonReader jsonReader = new JsonReader(new FileReader(resource.getFile()));
            return gson.fromJson(jsonReader, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // todo fix
            return null;
        }

    }

    public static List<Category> get(File file){
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<ArrayList<Category>>(){}.getType();
            JsonReader jsonReader = new JsonReader(new FileReader(file));
            return gson.fromJson(jsonReader, listType);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // todo fix
            return null;
        }

    }
}
