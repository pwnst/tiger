package com.tiger.app;

import java.util.Arrays;
import java.util.stream.Stream;

public class Category {

    private final String name;
    private final Question[] questions;

    public Category(String name, Question[] questions) {
        this.name = name;
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public Question[] getQuestions() {
        return questions;
    }

    public Stream<Question> questionStream() {
        return Arrays.stream(questions);
    }
}
